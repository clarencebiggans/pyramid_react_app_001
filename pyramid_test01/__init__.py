from pyramid.config import Configurator


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    with Configurator(settings=settings) as config:
        config.include('.models')
        config.include('pyramid_jinja2')
        config.include('.routes')
        # https://stackoverflow.com/questions/13606470/using-request-static-url-in-a-pyramid-app
        config.add_static_view(name='static', path='pyramid_test01:build/static/')
        config.add_static_view(name='static2', path='pyramid_test01:static/')
        config.scan()
    return config.make_wsgi_app()
